#![forbid(unsafe_code)]

use crate::audio::player::AudioPlayer;
use crate::audio::sound::Sound;
use crate::command_listeners::twitch_irc::TwitchIRCListener;
use crate::command_listeners::{Command, CommandListener};
use crate::config::Config;
use crate::tts::TTSConverterResolver;
use futures_util::future::join_all;
use std::collections::{HashMap, HashSet};
use std::path::Path;
use std::sync::Arc;
use tokio::sync::mpsc;

mod audio;
mod command_listeners;
mod config;
mod tts;

#[tokio::main]
async fn main() {
    let config =
        Config::read_from_file(Path::new("config.json")).expect("cannot read config.json file");

    let audio_player = AudioPlayer::new();

    let sounds = Arc::new(audio::loader::load_all_sounds(&config.sounds_path).await);
    let available_sounds: HashSet<String> = sounds.keys().cloned().collect();

    let tts_converter_container = Arc::new(TTSConverterResolver::new());

    let (tx, mut rx) = mpsc::channel::<Vec<Command>>(100);
    let listener = TwitchIRCListener::new(
        config.nick,
        config.channel,
        config.oauth,
        HashSet::new(), // TODO: auto reply not implemented yet
        available_sounds,
        tts_converter_container.get_supported_codes(),
    );
    tokio::spawn(async move { listener.run(tx).await });

    // TODO: refactor everything below
    while let Some(commands) = rx.recv().await {
        // TODO: remove, introduce some different logging
        println!("Received commands!");
        println!("{commands:?}");
        let should_queue = commands
            .iter()
            .any(|command| matches!(command, Command::TTS { .. }));
        let sound_futures = commands
            .into_iter()
            .filter(|command| match command {
                Command::Sound { .. } => true,
                Command::TTS { .. } => true,
                _ => false,
            })
            // TODO: some error if unresolvable sound leaked here?
            .map(|command| {
                tokio::spawn(transform_audible_command(
                    command,
                    sounds.clone(),
                    tts_converter_container.clone(),
                ))
            });

        // TODO: make sure that it's actually ran in parallel and not only concurrently
        let resolved_sounds = join_all(sound_futures)
            .await
            .into_iter()
            // TODO: replace unwrap, it indicates some spawn failure
            .filter_map(|sound| sound.unwrap())
            .collect();

        audio_player.play(resolved_sounds, should_queue);

        // TODO: implement support for autoreply and reset commands
    }
}

async fn transform_audible_command(
    command: Command,
    sounds: Arc<HashMap<String, Sound>>,
    tts_converter: Arc<TTSConverterResolver<'_>>,
) -> Option<Sound> {
    match command {
        Command::Sound { name, effects } => sounds.get(&name).map(|sound| {
            let mut sound = sound.clone();
            sound.apply_effects(&effects);
            sound
        }),
        Command::TTS {
            language_code,
            text,
            effects,
        } => tts_converter
            .convert_to_sound(&language_code, text.as_str())
            .await
            .map(|mut sound| {
                sound.apply_effects(&effects);
                sound
            }),
        _ => None,
    }
}
