use crate::audio::effects::Effect;
use crate::tts::TTSLanguageCode;
use tokio::sync::mpsc::Sender;

pub mod twitch_irc;

pub trait CommandListener {
    async fn run(&self, sender: Sender<Vec<Command>>);
}

#[derive(Eq, PartialEq, Debug)]
pub enum Command {
    AutoReply {
        request: String,
    },
    Sound {
        name: String,
        effects: Vec<Effect>,
    },
    TTS {
        language_code: TTSLanguageCode,
        text: String,
        effects: Vec<Effect>,
    },
    Reset,
}
