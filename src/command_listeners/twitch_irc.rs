use futures_util::{SinkExt, StreamExt};
use std::collections::HashSet;
use tokio::sync::mpsc;
use tokio::sync::mpsc::Sender;
use tokio_tungstenite::connect_async;
use tokio_tungstenite::tungstenite::Message;

use crate::command_listeners::twitch_irc::twitch_chat_command_parser::TwitchChatCommandParser;
use crate::command_listeners::twitch_irc::twitch_irc_message::TwitchIrcMessage;
use crate::command_listeners::{Command, CommandListener};
use crate::tts::TTSLanguageCode;

mod twitch_chat_command_parser;
mod twitch_irc_message;

const TWITCH_IRC_WS_URL: &str = "ws://irc-ws.chat.twitch.tv:80";

pub struct TwitchIRCListener {
    nick: String,
    channel: String,
    oauth: String,
    command_parser: TwitchChatCommandParser,
}

impl TwitchIRCListener {
    pub fn new(
        nick: String,
        channel: String,
        oauth: String,
        available_auto_reply_commands: HashSet<String>,
        available_sounds: HashSet<String>,
        available_tts_language_codes: HashSet<TTSLanguageCode>,
    ) -> TwitchIRCListener {
        TwitchIRCListener {
            nick,
            channel,
            oauth,
            command_parser: TwitchChatCommandParser::new(
                available_auto_reply_commands,
                available_sounds,
                available_tts_language_codes,
            ),
        }
    }
}

impl CommandListener for TwitchIRCListener {
    async fn run(&self, sender: Sender<Vec<Command>>) {
        // connect
        let (ws_stream, _) = connect_async(TWITCH_IRC_WS_URL)
            .await
            .expect("failed to connect to twitch IRC");
        let (mut write, read) = ws_stream.split();
        println!("Connected to twitch IRC!");

        // setup
        // TODO: use send_all?
        write.send(Message::text(format!("PASS {}", self.oauth))).await.unwrap();
        write.send(Message::text(format!("NICK {}", self.nick))).await.unwrap();
        write.send(Message::text(format!("JOIN #{}", self.channel))).await.unwrap();
        write.send(Message::text("CAP REQ :twitch.tv/tags")).await.unwrap();

        let (pong_tx, mut pong_rx) = mpsc::channel::<String>(2);
        tokio::spawn(async move {
            while let Some(pong) = pong_rx.recv().await {
                write.send(Message::text(pong)).await.unwrap();
            }
        });

        read.for_each(|message| async {
            // println!("Received message: {:?}", message);
            if let Ok(Message::Text(text)) = message {
                if text.starts_with("PING") {
                    let pong = text.replace("PING", "PONG");
                    pong_tx.send(pong).await.unwrap();
                } else {
                    let irc_message = TwitchIrcMessage::parse_from_raw_irc_string(text.as_str());
                    let commands = self.command_parser.convert_message_to_commands(irc_message);
                    if !commands.is_empty() {
                        sender.send(commands).await.unwrap(); // TODO: handle errors
                    }
                }
            } else {
                // TODO: look into these and handle
                println!("Unhandled message: {:?}", message);
            }
        }).await;

        // TODO: autoreply, get the write stream out of here?
    }
}
