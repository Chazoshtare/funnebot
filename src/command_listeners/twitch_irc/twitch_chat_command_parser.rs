use crate::audio::effects::Effect;
use crate::command_listeners::twitch_irc::twitch_irc_message::TwitchIrcMessage;
use crate::command_listeners::Command;
use crate::tts::TTSLanguageCode;
use std::collections::HashSet;
use std::str::FromStr;

const COMMAND_PREFIX: char = '!';
const COMMAND_SEPARATOR: &str = "+!";
const RESET_COMMAND: &str = "reset";

pub struct TwitchChatCommandParser {
    available_auto_reply_commands: HashSet<String>,
    available_sounds: HashSet<String>,
    available_tts_language_codes: HashSet<TTSLanguageCode>,
}

impl TwitchChatCommandParser {
    pub fn new(
        available_auto_reply_commands: HashSet<String>,
        available_sounds: HashSet<String>,
        available_tts_language_codes: HashSet<TTSLanguageCode>,
    ) -> Self {
        Self {
            available_auto_reply_commands,
            available_sounds,
            available_tts_language_codes,
        }
    }

    pub fn convert_message_to_commands(
        &self,
        twitch_irc_message: TwitchIrcMessage,
    ) -> Vec<Command> {
        let content = twitch_irc_message.content;
        let commands = if let Some(all_commands) = content.strip_prefix(COMMAND_PREFIX) {
            all_commands
                .split(COMMAND_SEPARATOR)
                .filter_map(|full_command| {
                    let (command_with_options, text) =
                        full_command.split_once(' ').unwrap_or((full_command, ""));
                    let (command, options) = command_with_options
                        .split_once('-')
                        .unwrap_or((command_with_options, ""));

                    // TODO: refactor to individual methods
                    if self.available_sounds.contains(command) {
                        let name = command;
                        let effects = Self::parse_options_to_effects(options);
                        Some(Command::Sound {
                            name: name.to_string(),
                            effects,
                        })
                    } else if self.supports_language(command) {
                        let language_code = TTSLanguageCode::new(command);
                        let effects = Self::parse_options_to_effects(options);
                        Some(Command::TTS {
                            language_code,
                            text: text.to_string(),
                            effects,
                        })
                    } else if self.available_auto_reply_commands.contains(command) {
                        Some(Command::AutoReply {
                            request: command.to_string(),
                        })
                    } else if RESET_COMMAND == command {
                        Some(Command::Reset)
                    } else {
                        None
                    }
                })
                .collect()
        } else {
            Vec::new()
        };

        commands
    }

    // TODO: implement and test for no conflicts too, like fast+slow
    fn parse_options_to_effects(options_string: &str) -> Vec<Effect> {
        options_string
            .split('-')
            .filter(|effect_name| !effect_name.is_empty())
            .filter_map(|effect_name| Effect::from_str(effect_name).ok())
            .fold(Vec::<Effect>::new(), |mut effects, new| {
                if effects.iter().all(|e| !e.conflicts_with(&new)) {
                    effects.push(new);
                    effects
                } else {
                    effects
                }
            })
    }

    fn supports_language(&self, language_code: &str) -> bool {
        self.available_tts_language_codes
            .contains(&TTSLanguageCode(language_code.to_lowercase()))
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use crate::audio::effects::Effect;
    use crate::command_listeners::twitch_irc::twitch_chat_command_parser::TwitchChatCommandParser;
    use crate::command_listeners::twitch_irc::twitch_irc_message::{
        IrcData, Source, Tags, TwitchIrcMessage,
    };
    use crate::command_listeners::Command;
    use crate::tts::TTSLanguageCode;

    fn create_test_message(content: &str) -> TwitchIrcMessage {
        TwitchIrcMessage {
            source: Source::empty(),
            irc_data: IrcData::empty(),
            tags: Tags::empty(),
            content: content.to_string(),
        }
    }

    #[test]
    fn parses_single_sound_command_from_twitch_irc_message() {
        let parser = TwitchChatCommandParser::new(
            HashSet::new(),
            HashSet::from(["sound".to_string()]),
            HashSet::new(),
        );
        let twitch_irc_message = create_test_message("!sound-er-c50");

        let commands = parser.convert_message_to_commands(twitch_irc_message);
        let expected_command = Command::Sound {
            name: "sound".to_string(),
            effects: vec![Effect::EarRape, Effect::Cut(Some(50))],
        };
        assert_eq!(commands, vec![expected_command]);
    }

    #[test]
    fn parses_single_tts_command_from_twitch_irc_message() {
        let parser = TwitchChatCommandParser::new(
            HashSet::new(),
            HashSet::new(),
            HashSet::from([TTSLanguageCode::new("us")]),
        );
        let twitch_irc_message = create_test_message("!us-s10-sk5 test message 2+2=4!");

        let commands = parser.convert_message_to_commands(twitch_irc_message);
        let expected_command = Command::TTS {
            language_code: TTSLanguageCode::new("us"),
            text: "test message 2+2=4!".to_string(),
            effects: vec![Effect::Slow(10), Effect::Skip(5)],
        };
        assert_eq!(commands, vec![expected_command]);
    }

    #[test]
    fn parses_single_autoreply_command_from_twitch_irc_message() {
        let parser = TwitchChatCommandParser::new(
            HashSet::from(["autoreply".to_string()]),
            HashSet::new(),
            HashSet::new(),
        );
        let twitch_irc_message = create_test_message("!autoreply-irrelevant rest of content");

        let commands = parser.convert_message_to_commands(twitch_irc_message);
        let expected_command = Command::AutoReply {
            request: "autoreply".to_string(),
        };
        assert_eq!(commands, vec![expected_command])
    }

    #[test]
    fn parses_single_reset_command_from_twitch_irc_message() {
        let parser = TwitchChatCommandParser::new(HashSet::new(), HashSet::new(), HashSet::new());
        let twitch_irc_message = create_test_message("!reset-irrelevant rest of content");

        let commands = parser.convert_message_to_commands(twitch_irc_message);
        assert_eq!(commands, vec![Command::Reset])
    }

    #[test]
    fn parses_multiple_tts_and_sound_commands_from_twitch_irc_message() {
        let parser = TwitchChatCommandParser::new(
            HashSet::new(),
            HashSet::from(["sound".to_string(), "laugh".to_string()]),
            HashSet::from([TTSLanguageCode::new("us"), TTSLanguageCode::new("gb")]),
        );
        let twitch_irc_message = create_test_message(
            "!us-er playing a laugh+!laugh-c20+!laugh-sk60+!gb-f and a sound+!sound-s5 some text",
        );

        let commands = parser.convert_message_to_commands(twitch_irc_message);
        let expected_commands = vec![
            Command::TTS {
                language_code: TTSLanguageCode::new("us"),
                text: "playing a laugh".to_string(),
                effects: vec![Effect::EarRape],
            },
            Command::Sound {
                name: "laugh".to_string(),
                effects: vec![Effect::Cut(Some(20))],
            },
            Command::Sound {
                name: "laugh".to_string(),
                effects: vec![Effect::Skip(60)],
            },
            Command::TTS {
                language_code: TTSLanguageCode::new("gb"),
                text: "and a sound".to_string(),
                effects: vec![Effect::Fast(30)],
            },
            Command::Sound {
                name: "sound".to_string(),
                effects: vec![Effect::Slow(5)],
            },
        ];
        assert_eq!(commands, expected_commands)
    }

    #[test]
    fn parses_effects_without_duplicates() {
        let parser = TwitchChatCommandParser::new(
            HashSet::new(),
            HashSet::from(["sound".to_string()]),
            HashSet::new(),
        );
        let twitch_irc_message = create_test_message("!sound-s10-r-s20-c-c50-r");

        let commands = parser.convert_message_to_commands(twitch_irc_message);
        let expected_command = Command::Sound {
            name: "sound".to_string(),
            effects: vec![Effect::Slow(10), Effect::Reverse, Effect::Cut(None)],
        };
        assert_eq!(commands, vec![expected_command]);
    }
}
