use std::collections::HashMap;

#[derive(Eq, PartialEq, Debug)]
pub struct TwitchIrcMessage {
    pub source: Source,
    pub irc_data: IrcData,
    pub tags: Tags,
    pub content: String,
}

#[derive(Eq, PartialEq, Debug)]
pub struct Tags {
    broadcaster: bool,
    moderator: bool,
    vip: bool,
    subscriber: bool,
}

#[derive(Eq, PartialEq, Debug)]
pub struct Source {
    nick: String,
    host: String,
}

#[derive(Eq, PartialEq, Debug)]
pub struct IrcData {
    irc_command: String,
    channel: String,
}

impl TwitchIrcMessage {
    // TODO: more tests
    pub fn parse_from_raw_irc_string(raw_irc_string: &str) -> Self {
        /*
        Example full strings:
        @badge-info=subscriber/43;badges=broadcaster/1,subscriber/3012;client-nonce=29185e4ab41f8679eeffa1a97d560ff9;color=#FF69B4;display-name=Chazoshtare;emotes=;first-msg=0;flags=;id=76a9354a-1dd9-4fd8-acf2-8416ffb2fac0;mod=0;room-id=87486142;subscriber=1;tmi-sent-ts=1636936451875;turbo=0;user-id=87486142;user-type= :chazoshtare!chazoshtare@chazoshtare.tmi.twitch.tv PRIVMSG #chazoshtare :some test message
        @badge-info=;badges=moderator/1,partner/1;color=#5B99FF;display-name=StreamElements;emotes=;first-msg=0;flags=;id=d516931b-200a-42ae-b439-51c72c30d883;mod=1;room-id=87486142;subscriber=0;tmi-sent-ts=1636936504551;turbo=0;user-id=100135110;user-type=mod :streamelements!streamelements@streamelements.tmi.twitch.tv PRIVMSG #chazoshtare :@Chazoshtare, missing youtube ID, url or search
        */
        let mut index = 1;

        // tags
        let tags = if raw_irc_string.starts_with('@') {
            let tags_end_index = raw_irc_string.find(' ').expect("invalid twitch IRC string");
            let raw_tags_string = &raw_irc_string[index..tags_end_index];
            index = tags_end_index + 1;
            Tags::parse_from(raw_tags_string)
        } else {
            Tags::empty()
        };

        // source
        let raw_irc_string = &raw_irc_string[index..];
        // :chazoshtare!chazoshtare@chazoshtare.tmi.twitch.tv PRIVMSG #chazoshtare :some test message
        let source = if raw_irc_string.starts_with(':') {
            let source_end_index = raw_irc_string.find(' ').expect("invalid twitch IRC string");
            let raw_source_string = &raw_irc_string[1..source_end_index];
            index = source_end_index + 1;
            Source::parse_from(raw_source_string)
        } else {
            Source::empty()
        };

        // ircData
        let raw_irc_string = &raw_irc_string[index..];
        // PRIVMSG #chazoshtare :some test message
        let irc_data_end_index = raw_irc_string.find(':').unwrap_or_else(|| raw_irc_string.len());
        let raw_irc_data_string = &raw_irc_string[0..irc_data_end_index].trim_end();
        let irc_data = IrcData::parse_from(raw_irc_data_string);

        // content
        let content = if irc_data_end_index != (raw_irc_string.len()) {
            index = irc_data_end_index + 1;
            &raw_irc_string[index..].trim()
        } else {
            ""
        };

        Self { source, irc_data, tags, content: content.to_string() }
    }
}

impl Tags {
    // TODO: more tests
    fn parse_from(raw_tags_string: &str) -> Self {
        // badge-info=subscriber/43;badges=broadcaster/1,subscriber/3012;client-nonce=29185e4ab41f8679eeffa1a97d560ff9;color=#FF69B4;display-name=Chazoshtare;emotes=;first-msg=0;flags=;id=76a9354a-1dd9-4fd8-acf2-8416ffb2fac0;mod=0;room-id=87486142;subscriber=1;tmi-sent-ts=1636936451875;turbo=0;user-id=87486142;user-type=
        let parsed_tags: HashMap<&str, &str> = raw_tags_string.split(';')
            .filter_map(|tag_with_value| tag_with_value.split_once('='))
            .collect();
        let badges = parsed_tags.get("badges").unwrap_or(&"");
        let parsed_badges: HashMap<&str, &str> = badges.split(',')
            .filter_map(|badge_with_value| badge_with_value.split_once('/'))
            .collect();

        let broadcaster = parsed_badges.get("broadcaster").map_or(false, |value| value == &"1");
        let moderator = parsed_tags.get("mod").map_or(false, |value| value == &"1");
        let vip = parsed_badges.get("vip").map_or(false, |value| value == &"1");
        let subscriber = parsed_tags.get("subscriber").map_or(false, |value| value == &"1");

        Self { broadcaster, moderator, vip, subscriber }
    }

    pub fn empty() -> Self {
        Self {
            broadcaster: false,
            moderator: false,
            vip: false,
            subscriber: false,
        }
    }
}

impl Source {
    fn parse_from(raw_source_string: &str) -> Self {
        // chazoshtare!chazoshtare@chazoshtare.tmi.twitch.tv
        raw_source_string.split_once('!')
            .map_or_else(
                Self::empty,
                |nick_with_host| Self {
                    nick: nick_with_host.0.to_string(),
                    host: nick_with_host.1.to_string(),
                },
            )
    }

    pub fn empty() -> Self {
        Self {
            nick: "".to_string(),
            host: "".to_string(),
        }
    }
}

impl IrcData {
    fn parse_from(raw_irc_data_string: &str) -> Self {
        // PRIVMSG #chazoshtare
        raw_irc_data_string.split_once(' ')
            .map_or_else(
                Self::empty,
                |irc_command_with_channel| Self {
                    irc_command: irc_command_with_channel.0.to_string(),
                    channel: irc_command_with_channel.1.trim_start_matches('#').to_string(),
                },
            )
    }

    pub fn empty() -> Self {
        Self {
            irc_command: "".to_string(),
            channel: "".to_string(),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::command_listeners::twitch_irc::twitch_irc_message::{IrcData, Source, Tags, TwitchIrcMessage};

    #[test]
    fn parses_twitch_irc_message_from_raw_irc_string() {
        let raw_irc_string =
            "@badge-info=subscriber/43;badges=broadcaster/1,subscriber/3012;\
            client-nonce=29185e4ab41f8679eeffa1a97d560ff9;color=#FF69B4;display-name=Chazoshtare;\
            emotes=;first-msg=0;flags=;id=76a9354a-1dd9-4fd8-acf2-8416ffb2fac0;mod=0;\
            room-id=87486142;subscriber=1;tmi-sent-ts=1636936451875;turbo=0;user-id=87486142;user-type= \
            :chazoshtare!chazoshtare@chazoshtare.tmi.twitch.tv PRIVMSG #chazoshtare :some test message";
        let twitch_irc_message = TwitchIrcMessage::parse_from_raw_irc_string(raw_irc_string);

        let expected_twitch_irc_message = TwitchIrcMessage {
            source: Source {
                nick: "chazoshtare".to_string(),
                host: "chazoshtare@chazoshtare.tmi.twitch.tv".to_string(),
            },
            irc_data: IrcData {
                irc_command: "PRIVMSG".to_string(),
                channel: "chazoshtare".to_string(),
            },
            tags: Tags {
                broadcaster: true,
                moderator: false,
                vip: false,
                subscriber: true,
            },
            content: "some test message".to_string(),
        };
        assert_eq!(twitch_irc_message, expected_twitch_irc_message);
    }

    #[test]
    fn parses_broadcaster_tags_from_raw_tags_string() {
        let raw_tags_string =
            "badge-info=subscriber/43;badges=broadcaster/1,subscriber/3012;\
            client-nonce=29185e4ab41f8679eeffa1a97d560ff9;color=#FF69B4;display-name=Chazoshtare;\
            emotes=;first-msg=0;flags=;id=76a9354a-1dd9-4fd8-acf2-8416ffb2fac0;mod=0;\
            room-id=87486142;subscriber=1;tmi-sent-ts=1636936451875;turbo=0;user-id=87486142;user-type=";
        let tags = Tags::parse_from(raw_tags_string);

        let expected_tags = Tags {
            broadcaster: true,
            moderator: false,
            vip: false,
            subscriber: true,
        };
        assert_eq!(tags, expected_tags);
    }

    #[test]
    fn parses_source_from_raw_source_string() {
        let raw_source_string = "chazoshtare!chazoshtare@chazoshtare.tmi.twitch.tv";
        let source = Source::parse_from(raw_source_string);

        let expected_source = Source {
            nick: "chazoshtare".to_string(),
            host: "chazoshtare@chazoshtare.tmi.twitch.tv".to_string(),
        };
        assert_eq!(source, expected_source);
    }

    #[test]
    fn returns_empty_source_if_raw_source_string_is_unparseable() {
        let raw_source_string = "some_erroneous_string";
        let source = Source::parse_from(raw_source_string);
        assert_eq!(source, Source::empty());
    }

    #[test]
    fn parses_irc_data_from_raw_irc_data_string() {
        let raw_irc_data_string = "PRIVMSG #chazoshtare";
        let irc_data = IrcData::parse_from(raw_irc_data_string);

        let expected_irc_data = IrcData {
            irc_command: "PRIVMSG".to_string(),
            channel: "chazoshtare".to_string(),
        };
        assert_eq!(irc_data, expected_irc_data);
    }

    #[test]
    fn returns_empty_irc_data_if_raw_irc_data_string_is_unparseable() {
        let raw_irc_data_string = "some_erroneous_string";
        let irc_data = IrcData::parse_from(raw_irc_data_string);
        assert_eq!(irc_data, IrcData::empty());
    }
}
