use crate::audio::effects::Effect::{Cut, EarRape, Fast, Reverb, Reverse, Skip, Slow};
use crate::audio::sound::Sound;
use crate::audio::DEFAULT_SAMPLE_RATE;
use mverb_rs::MVerb;
use std::str::FromStr;
use std::{iter, mem};

const FOUR_SECONDS_SILENCE: [f32; (DEFAULT_SAMPLE_RATE * 8) as usize] =
    [0f32; (DEFAULT_SAMPLE_RATE * 8) as usize];

#[derive(Eq, PartialEq, Debug)]
pub enum Effect {
    Cut(Option<u16>),
    EarRape,
    Fast(u16),
    Reverb,
    Reverse,
    Skip(u16),
    Slow(u16),
    // TODO: PitchShift
}

impl Effect {
    pub fn apply_on(&self, sound: &mut Sound) {
        match self {
            Cut(level) => {
                let samples_length = sound.samples.len();
                let cut_point = if level.is_none() && samples_length >= sound.sample_rate as usize {
                    (sound.sample_rate / 2) as usize
                } else {
                    let level = level.unwrap_or(50).clamp(1, 99) as usize;
                    level * samples_length / 100
                };
                let unpaired_samples = cut_point % sound.channels as usize;
                let end = if unpaired_samples == 0 {
                    cut_point
                } else {
                    cut_point + (sound.channels as usize - unpaired_samples)
                };
                sound.samples.truncate(end);
            }
            EarRape => {
                // TODO: extract magic numbers
                sound.change_volume(40f32);
                let new_peak = sound.highest_peak();
                let deamplify_factor = sound.original_peak() / new_peak;
                sound.change_volume(deamplify_factor * 0.17f32)
            }
            Fast(level) => {
                // "speeds up" the sound by increasing the sample rate then resampling back
                let level = *level.clamp(&1, &50) as u32;
                let sample_rate = sound.sample_rate * 100 / (100 - level);
                sound.sample_rate = sample_rate;
                sound.resample(DEFAULT_SAMPLE_RATE);
            }
            Reverb => {
                let mut reverb = MVerb::default();
                reverb.set_sample_rate(DEFAULT_SAMPLE_RATE as f32);
                let samples = sound
                    .samples
                    .chunks(2)
                    .chain(FOUR_SECONDS_SILENCE.chunks(2))
                    .flat_map(|frame| {
                        let (a, b) = reverb.process((frame[0], frame[1]));
                        iter::once(a).chain(iter::once(b))
                    })
                    .collect();
                sound.samples = samples;
            }
            Reverse => {
                let samples = sound
                    .samples
                    .rchunks(sound.channels as usize)
                    .flatten()
                    .map(|sample| *sample)
                    .collect::<Vec<f32>>();
                sound.samples = samples;
            }
            Skip(level) => {
                let level = *level.clamp(&1, &99) as usize;
                let start_point = level * sound.samples.len() / 100;
                let unpaired_samples = start_point % sound.channels as usize;
                let start = if unpaired_samples == 0 {
                    start_point
                } else {
                    start_point - (sound.channels as usize - unpaired_samples)
                };
                sound.samples.drain(..start);
            }
            Slow(level) => {
                // "slows down" the sound by reducing the sample rate then resampling back
                let level = *level.clamp(&1, &50) as u32;
                let sample_rate = sound.sample_rate * 100 / (100 + level);
                sound.sample_rate = sample_rate;
                sound.resample(DEFAULT_SAMPLE_RATE);
            } // _ => {}
        }
    }

    pub fn conflicts_with(&self, other: &Effect) -> bool {
        mem::discriminant(self) == mem::discriminant(other)
        // TODO: also disallow fast with slow
    }
}

impl FromStr for Effect {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let name: String = s.chars().take_while(|char| char.is_alphabetic()).collect();
        let level_str: String = s
            .chars()
            .skip_while(|char| !char.is_digit(10))
            .take_while(|char| char.is_digit(10))
            .collect();
        let level: Option<u16> = if level_str.is_empty() {
            None
        } else {
            match level_str.parse::<u16>() {
                Ok(value) => Some(value),
                Err(_) => None,
            }
        };

        match name.as_str() {
            "c" | "cut" => Ok(Cut(level)),
            "er" | "earrape" => Ok(EarRape),
            "f" | "fast" => Ok(Fast(level.unwrap_or(30))),
            "rv" | "reverb" => Ok(Reverb),
            "r" | "reverse" => Ok(Reverse),
            "sk" | "skip" => Ok(Skip(level.unwrap_or(50))),
            "s" | "slow" => Ok(Slow(level.unwrap_or(30))),
            _ => Err(()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parses_effect_without_level_from_string() {
        let string = "reverse";
        let effect = Effect::from_str(string);
        assert_eq!(effect, Ok(Reverse));
    }

    #[test]
    fn parses_effect_with_level_from_string() {
        let string = "cut50";
        let effect = Effect::from_str(string);
        assert_eq!(effect, Ok(Cut(Some(50))));
    }

    #[test]
    fn uses_default_level_for_effect_if_outside_number_bounds() {
        let string = "slow99999999999";
        let effect = Effect::from_str(string);
        assert_eq!(effect, Ok(Slow(30)));
    }

    #[test]
    fn parses_no_effect_from_str() {
        let string = "invalid123";
        let effect = Effect::from_str(string);
        assert_eq!(effect, Err(()));
    }

    #[test]
    fn reverse_keeps_channel_order() {
        let samples = vec![0.1, 0.2, 0.3, 0.4, 0.5, 0.6];
        let mut sound = Sound::new(2, 44100, samples);

        Reverse.apply_on(&mut sound);

        let expected_samples = vec![0.5, 0.6, 0.3, 0.4, 0.1, 0.2];
        let expected = Sound::new(2, 44100, expected_samples);
        assert_eq!(sound, expected);
    }

    #[test]
    fn cut_does_not_leave_unpaired_samples() {
        let samples = vec![0.1, 0.2, 0.3, 0.4, 0.5, 0.6];
        let mut sound = Sound::new(2, 44100, samples);
        let effect = Cut(Some(50));

        effect.apply_on(&mut sound);

        let expected_samples = vec![0.1, 0.2, 0.3, 0.4];
        let expected = Sound::new(2, 44100, expected_samples);
        assert_eq!(sound, expected);
    }

    #[test]
    fn cut_defaults_to_half_if_sound_is_shorter_than_one_second() {
        let samples = vec![0.1, 0.2, 0.3, 0.4];
        let mut sound = Sound::new(2, 44100, samples);
        let effect = Cut(None);

        effect.apply_on(&mut sound);

        let expected_samples = vec![0.1, 0.2];
        let expected = Sound::new(2, 44100, expected_samples);
        assert_eq!(sound, expected);
    }

    #[test]
    fn cut_defaults_to_half_a_second_if_sound_is_at_least_one_second() {
        let samples = vec![0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.10];
        let mut sound = Sound::new(2, 8, samples);
        let effect = Cut(None);

        effect.apply_on(&mut sound);

        let expected_samples = vec![0.1, 0.2, 0.3, 0.4];
        let expected = Sound::new(2, 8, expected_samples);
        assert_eq!(sound, expected);
    }

    #[test]
    fn skip_does_not_leave_unpaired_samples() {
        let samples = vec![0.1, 0.2, 0.3, 0.4, 0.5, 0.6];
        let mut sound = Sound::new(2, 44100, samples);
        let effect = Skip(50);

        effect.apply_on(&mut sound);

        let expected_samples = vec![0.3, 0.4, 0.5, 0.6];
        let expected = Sound::new(2, 44100, expected_samples);
        assert_eq!(sound, expected);
    }
}
