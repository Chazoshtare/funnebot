use crate::audio::effects::Effect;
use bytes::Bytes;
use rodio::buffer::SamplesBuffer;
use rodio::decoder::DecoderError;
use rodio::{Decoder, Source};
use rubato::{FastFixedIn, PolynomialDegree, Resampler};
use std::error::Error;
use std::fs::File;
use std::io::Cursor;
use std::iter;
use std::path::Path;

#[derive(Clone, Debug, PartialEq)]
pub struct Sound {
    pub channels: u16,
    pub sample_rate: u32,
    pub samples: Vec<f32>,
    original_peak: f32,
}

impl Sound {
    pub fn new(channels: u16, sample_rate: u32, samples: Vec<f32>) -> Self {
        let original_peak = highest_peak(&samples);
        Self {
            channels,
            sample_rate,
            samples,
            original_peak,
        }
    }

    pub fn from_file(path: &Path) -> Result<Self, Box<dyn Error>> {
        let file = File::open(path)?;
        let source = Decoder::new(file)?.convert_samples();
        Ok(Self::new(
            source.channels(),
            source.sample_rate(),
            source.collect(),
        ))
    }

    pub fn from_bytes(bytes: Bytes) -> Result<Self, DecoderError> {
        let cursor = Cursor::new(bytes);
        let source = Decoder::new(cursor)?.convert_samples();
        Ok(Self::new(
            source.channels(),
            source.sample_rate(),
            source.collect(),
        ))
    }

    pub fn apply_effects(&mut self, effects: &[Effect]) {
        effects.iter().for_each(|effect| effect.apply_on(self));
    }

    pub fn change_volume(&mut self, factor: f32) {
        if factor != 1.0 {
            let samples = self
                .samples
                .iter()
                .map(|sample| *sample * factor)
                .map(|sample| sample.clamp(-1.0, 1.0))
                .collect();
            self.samples = samples;
        }
    }

    pub fn convert_to_stereo(&mut self) {
        match self.channels {
            1 => {
                let samples: Vec<f32> = self
                    .samples
                    .iter()
                    .flat_map(|sample| iter::once(sample).chain(iter::once(sample)))
                    .map(|sample| *sample)
                    .collect();
                self.samples = samples;
                self.channels = 2;
            }
            2 => {}
            other => {
                eprintln!("Unsupported channel count for stereo conversion: {}", other);
            }
        }
    }

    pub fn resample(&mut self, target_sample_rate: u32) {
        if self.sample_rate != target_sample_rate {
            let channels = self.channels as usize;
            let resampling_ratio = target_sample_rate as f64 / self.sample_rate as f64;
            let mut resampler = FastFixedIn::<f32>::new(
                resampling_ratio,
                1.1,
                PolynomialDegree::Septic,
                1024,
                channels,
            )
            .unwrap();

            let output_delay = resampler.output_delay();
            let resampled_length =
                (self.frames_len() as f64 * resampling_ratio) as usize + output_delay;
            let mut full_resample_output: Vec<Vec<f32>> =
                vec![Vec::with_capacity(resampled_length); channels];
            let mut output_buffer = vec![vec![0.0f32; resampler.output_frames_max()]; channels];

            let mut required_input_frames = resampler.input_frames_next();
            let deinterleaved_samples = deinterleave(&self.samples, channels);
            let mut input_data_slices: Vec<&[f32]> =
                deinterleaved_samples.iter().map(|v| &v[..]).collect();
            while input_data_slices[0].len() >= required_input_frames {
                let (number_in, number_out) = resampler
                    .process_into_buffer(&input_data_slices, &mut output_buffer, None)
                    .unwrap();
                for channel_data in input_data_slices.iter_mut() {
                    *channel_data = &channel_data[number_in..];
                }
                for channel in 0..channels {
                    full_resample_output[channel]
                        .extend_from_slice(&output_buffer[channel][0..number_out]);
                }
                required_input_frames = resampler.input_frames_next();
            }

            if !input_data_slices[0].is_empty() {
                let (_, number_out) = resampler
                    .process_partial_into_buffer(Some(&input_data_slices), &mut output_buffer, None)
                    .unwrap();
                for channel in 0..channels {
                    full_resample_output[channel]
                        .extend_from_slice(&output_buffer[channel][0..number_out]);
                }
            }

            let interleaved = interleave(full_resample_output, output_delay);
            self.sample_rate = target_sample_rate;
            self.samples = interleaved;
        }
    }

    pub fn highest_peak(&self) -> f32 {
        highest_peak(&self.samples[..])
    }

    pub fn original_peak(&self) -> f32 {
        self.original_peak
    }

    pub fn to_samples_buffer(self) -> SamplesBuffer<f32> {
        SamplesBuffer::new(self.channels, self.sample_rate, self.samples)
    }

    fn frames_len(&self) -> usize {
        self.samples.len() / self.channels as usize
    }
}

fn highest_peak(samples: &[f32]) -> f32 {
    samples.iter().fold(0.0, |a, b| a.max(*b))
}

// TODO: maybe not required, instead of deinterleaving return an iterator to chunks - less waste
fn deinterleave(samples: &Vec<f32>, channels: usize) -> Vec<Vec<f32>> {
    let mut output = vec![vec![]; channels];
    samples.chunks_exact(channels).for_each(|chunk| {
        for i in 0..channels {
            output[i].push(chunk[i])
        }
    });
    output
}

// TODO: maybe not required, add already interleaved to full_resample_output instead of this
fn interleave(samples: Vec<Vec<f32>>, skip_frames_number: usize) -> Vec<f32> {
    let channels = samples.len();
    let frames = samples[0].len();
    let mut output = Vec::with_capacity((frames - skip_frames_number) * channels);
    for frame in skip_frames_number..frames {
        for channel in 0..channels {
            output.push(samples[channel][frame]);
        }
    }
    output
}

#[cfg(test)]
mod tests {
    use crate::audio::sound::Sound;

    #[test]
    fn converts_mono_sound_to_stereo() {
        let samples = vec![0.1, 0.2, 0.3];
        let mut sound = Sound::new(1, 44100, samples);

        sound.convert_to_stereo();

        let expected_samples = vec![0.1, 0.1, 0.2, 0.2, 0.3, 0.3];
        let expected_sound = Sound::new(2, 44100, expected_samples);
        assert_eq!(sound, expected_sound);
    }

    #[test]
    fn leaves_stereo_sound_intact_while_converting_to_stereo() {
        let samples = vec![0.1, 0.2, 0.3, 0.4];
        let mut sound = Sound::new(2, 44100, samples);

        sound.convert_to_stereo();

        let expected_samples = vec![0.1, 0.2, 0.3, 0.4];
        let expected_sound = Sound::new(2, 44100, expected_samples);
        assert_eq!(sound, expected_sound);
    }
}
