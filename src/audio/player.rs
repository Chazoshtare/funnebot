use crate::audio::sound::Sound;
use rodio::{OutputStream, OutputStreamHandle, Sink};

pub struct AudioPlayer {
    _output_stream: OutputStream,
    output_stream_handle: OutputStreamHandle,
    queue_sink: Sink,
}

impl AudioPlayer {
    // TODO: ensure stream is opened with DEFAULT_SAMPLE_RATE
    pub fn new() -> AudioPlayer {
        let (_stream, stream_handle) =
            OutputStream::try_default().expect("cannot open default audio output");
        let sink = Sink::try_new(&stream_handle).expect("error while creating a new sink");
        AudioPlayer {
            _output_stream: _stream,
            output_stream_handle: stream_handle,
            queue_sink: sink,
        }
    }

    pub fn play(&self, sounds: Vec<Sound>, queue: bool) {
        if queue {
            self.play_queued(sounds);
        } else {
            self.play_immediately(sounds);
        }
    }

    fn play_immediately(&self, sounds: Vec<Sound>) {
        let sink =
            Sink::try_new(&self.output_stream_handle).expect("error while creating a new sink");
        sounds
            .into_iter()
            .map(|sound| sound.to_samples_buffer())
            .for_each(|sound| sink.append(sound));
        sink.detach();
    }

    fn play_queued(&self, sounds: Vec<Sound>) {
        sounds
            .into_iter()
            .map(|sound| sound.to_samples_buffer())
            .for_each(|sound| self.queue_sink.append(sound));
    }
}
