use crate::audio::sound::Sound;
use crate::audio::DEFAULT_SAMPLE_RATE;
use std::collections::HashMap;
use std::fs;
use std::path::Path;
use tokio::task::JoinSet;

pub async fn load_all_sounds(root_path: &Path) -> HashMap<String, Sound> {
    let dir_contents = match fs::read_dir(root_path) {
        Ok(dir_contents) => dir_contents,
        Err(_) => {
            eprintln!("cannot read sound directory: {root_path:?}");
            return HashMap::new();
        }
    };

    let mut join_set = JoinSet::new();
    dir_contents.for_each(|path| match path {
        Ok(dir_entry) => {
            join_set.spawn(async move { load_sound(dir_entry.path().as_path()) });
        }
        Err(ref error) => {
            eprintln!("error while reading file {path:?}, {error} - ignoring");
        }
    });
    join_set
        .join_all()
        .await
        .into_iter()
        .filter_map(|loaded| loaded)
        .collect()
}

fn load_sound(path: &Path) -> Option<(String, Sound)> {
    let name = match get_sound_name(path) {
        Some(sound_name) => sound_name,
        None => {
            eprintln!("error while parsing sound name {path:?} - ignoring");
            return None;
        }
    };

    let mut sound = match Sound::from_file(path) {
        Ok(sound) => sound,
        Err(error) => {
            eprintln!("error while loading sound {path:?}, {error} - ignoring");
            return None;
        }
    };
    sound.resample(DEFAULT_SAMPLE_RATE);
    sound.convert_to_stereo();
    Some((name.to_string(), sound))
}

fn get_sound_name(path: &Path) -> Option<&str> {
    path.file_name()
        .map(|file_name| file_name.to_str())
        .flatten()
        .map(|name_with_extension| {
            name_with_extension
                .rsplit_once('.')
                .map(|(name, _)| name)
                .unwrap_or(name_with_extension)
        })
}
