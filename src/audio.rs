pub mod effects;
pub mod loader;
pub mod player;
pub mod sound;

// TODO: use to open default stream
pub const DEFAULT_SAMPLE_RATE: u32 = 44100;
