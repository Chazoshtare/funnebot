use std::collections::{HashMap, HashSet};
use std::error::Error;

use crate::audio::sound::Sound;
use crate::audio::DEFAULT_SAMPLE_RATE;
use crate::tts::{TTSConverter, TTSLanguageCode};
use reqwest::Client;
use url::Url;

const GOOGLE_TTS_BASE_URL: &str = "https://translate.google.com/translate_tts";

pub struct GoogleTTSConverter<'a> {
    supported_codes_with_google_values: HashMap<&'a str, &'a str>,
    client: Client,
}

impl GoogleTTSConverter<'_> {
    pub fn new<'a>() -> GoogleTTSConverter<'a> {
        GoogleTTSConverter {
            supported_codes_with_google_values: HashMap::from([
                ("en", "en_US"), ("us", "en_US"), ("gb", "en_GB"), ("au", "en_AU"),
                ("it", "it_IT"), ("pl", "pl_PL"), ("uk", "uk_UA"), ("vi", "vi_VN"),
                ("jp", "ja_JP"), ("dk", "da_DK"), ("nl", "nl_NL"), ("fi", "fi_FI"),
                ("ca", "fr_CA"), ("fr", "fr_FR"), ("de", "de_DE"), ("kr", "ko_KR"),
                ("no", "nb_NO"), ("pt", "pt_PT"), ("ru", "ru_RU"), ("sk", "sk_SK"),
                ("sv", "sv_SE"), ("tr", "tr_TR"), ("ar", "es_AR"), ("in", "en_IN"),
                ("hi", "hi_IN"), ("cn", "cmn_TW"), ("is", "is_IS"), ("af", "af_ZA"),
                ("cz", "cs-CZ"), ("sr", "sr-RS")
            ]),
            client: Client::new(),
        }
    }
}

impl TTSConverter for GoogleTTSConverter<'_> {
    fn get_supported_codes(&self) -> HashSet<TTSLanguageCode> {
        self.supported_codes_with_google_values.iter()
            .map(|codes| { TTSLanguageCode(codes.0.to_string()) })
            .collect()
    }

    fn supports_language_code(&self, language_code: &TTSLanguageCode) -> bool {
        self.supported_codes_with_google_values.contains_key(language_code.0.as_str())
    }

    async fn convert_to_sound(&self, language_code: &TTSLanguageCode, text: &str) -> Result<Sound, Box<dyn Error>> {
        let code = self.supported_codes_with_google_values.get(language_code.0.as_str()).unwrap_or(&"en_US");
        let params = [
            ("client", "tw-ob"),
            ("q", text),
            ("tl", code)
        ];
        let url = Url::parse_with_params(GOOGLE_TTS_BASE_URL, params)?;
        let bytes = self.client.get(url)
            .send().await?
            .bytes().await?;
        let mut sound = Sound::from_bytes(bytes)?;
        sound.resample(DEFAULT_SAMPLE_RATE);
        sound.convert_to_stereo();
        Ok(sound)
    }
}
