use crate::audio::sound::Sound;
use crate::tts::google_tts_converter::GoogleTTSConverter;
use std::collections::HashSet;
use std::error::Error;

mod google_tts_converter;

trait TTSConverter {
    fn get_supported_codes(&self) -> HashSet<TTSLanguageCode>;

    fn supports_language_code(&self, language_code: &TTSLanguageCode) -> bool;

    async fn convert_to_sound(
        &self,
        language_code: &TTSLanguageCode,
        text: &str,
    ) -> Result<Sound, Box<dyn Error>>;
}

#[derive(Eq, PartialEq, Debug, Hash)]
pub struct TTSLanguageCode(pub String);

impl TTSLanguageCode {
    pub fn new(code: &str) -> TTSLanguageCode {
        TTSLanguageCode(code.to_string())
    }
}

// TODO: temporarily all contained as fields, trait TTSConverter cannot be made into an object because of async
pub struct TTSConverterResolver<'a> {
    google_tts_converter: GoogleTTSConverter<'a>,
}

impl TTSConverterResolver<'_> {
    pub fn new() -> Self {
        Self {
            google_tts_converter: GoogleTTSConverter::new(),
        }
    }

    pub fn get_supported_codes(&self) -> HashSet<TTSLanguageCode> {
        self.google_tts_converter.get_supported_codes()
    }

    // TODO: return result instead of option?
    pub async fn convert_to_sound(
        &self,
        language_code: &TTSLanguageCode,
        text: &str,
    ) -> Option<Sound> {
        if self
            .google_tts_converter
            .supports_language_code(language_code)
        {
            let tts_sound_data = self
                .google_tts_converter
                .convert_to_sound(language_code, text)
                .await;
            match tts_sound_data {
                Ok(sound_data) => Some(sound_data),
                Err(error) => {
                    eprintln!("error while converting TTS: {error}");
                    None
                }
            }
        } else {
            None
        }
    }
}
