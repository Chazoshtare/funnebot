use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::path::{Path, PathBuf};

use serde::Deserialize;

#[derive(Deserialize)]
pub struct Config {
    pub nick: String,
    pub channel: String,
    pub oauth: String,
    pub commands_path: PathBuf,
    pub sounds_path: PathBuf,
}

impl Config {
    pub fn read_from_file(path: &Path) -> Result<Config, Box<dyn Error>> {
        let file = File::open(path)?;
        let reader = BufReader::new(file);
        let config = serde_json::from_reader(reader)?;
        Ok(config)
    }
}
