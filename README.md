# Funnebot

Twitch chatbot for all extremely funny things, like playing sounds or TTS with custom effects and
possibly much more.
This is a rewrite of
previous [Kotlin/Native version of Funnebot](https://gitlab.com/Chazoshtare/funnebot-legacy).  
**This is a work in progress, there are many things that are missing, and possibly undiscovered
bugs.**

## Features

**Playing sounds:**

- `![sound]` - to play a sound
- `![sound]-[effect]` - to play a sound with effect

**Playing TTS** - you need to send language code and a message:

- `![voice] message` - to play TTS
- `![voice]-[effect] message` - to play TTS with effect

Effects available: cut (c), earrape (er), fast (f), reverb (rv), reverse (r), skip (sk), slow (s),
more in the future.  
Cut, skip, fast and slow can be adjusted with a number, for example, c70 cuts 70% of the sound, s50
slows the sound by 50%. 50% is max for speed changes. 99% is max for cut and skip.

For example `!us-fast This is a super funny TTS message!!!!`  
You can chain multiple effects, for example: `!sound-er-f50-c80`  
You can chain multiple sounds and TTSs, for example: `!sound-er-f+!us-s Hey there+!sound`

Currently, only Google TTS is supported.

## Running

Create a config file right next to funnebot executable called `config.json`. It
needs to look like this:

```json
{
  "nick": "[your-or-bot's-nick]",
  "channel": "[channel-to-join]",
  "oauth": "[oauth:XXXXXXXXX]",
  "commands_path": "[path-to-commands-folder]",
  "sounds_path": "[path-to-sounds-folder]"
}
```

You can generate `oauth` token here: https://twitchtokengenerator.com/

## Building

This is written in Rust, so you can build it Cargo:

```shell
cargo build
```

## TODO

Among other things:

- Tetyys and IMTranslator TTSs
- auto-respond commands
- reset command
- PitchShift effect
- Easily downloadable releases and auto-builds
